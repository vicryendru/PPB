List<int> range(int start, int end) {
  List<int> result = [];

  if (start <= end) {
    for (int i = start; i <= end; i++) {
      result.add(i);
    }
  } else {
    for (int i = start; i >= end; i--) {
      result.add(i);
    }
  }

  return result;
}

void main() {
  List<int> ascending = range(1, 5);
  List<int> descending = range(5, 1);

  print(ascending);
  print(descending);
}
