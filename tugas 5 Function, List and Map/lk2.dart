int perkalian(int num1, int num2) {
  return num1 * num2;
}

void main() {
  var num1 = 12;
  var num2 = 4;
  var hasilKali = perkalian(num1, num2);
  print(hasilKali); // Output: 48
}
