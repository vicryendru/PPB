void dataHandling(List<List<String>> data) {
  for (var i = 0; i < data.length; i++) {
    var id = data[i][0];
    var name = data[i][1];
    var city = data[i][2];
    var dob = data[i][3];
    var hobby = data[i][4];

    print('Nomor ID: $id');
    print('Nama Lengkap: $name');
    print('TTL: $city $dob');
    print('Hobi $hobby\n');
  }
}

void main() {
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winoma", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
  ];

  dataHandling(input);
}
