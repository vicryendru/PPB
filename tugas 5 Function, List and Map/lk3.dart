String salamKenal(String name, int age, String address, String hobby) {
  return 'Nama saya $name, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!';
}

void main() {
  var name = "Endru";
  var age = 17;
  var address = "Karawang";
  var hobby = "Gaming";
  var perkenalan = salamKenal(name, age, address, hobby);
  print(perkenalan);
}
