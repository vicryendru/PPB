List<int> range(int start, int finish) {
  if (start <= finish) {
    return List.generate(finish - start + 1, (i) => start + i);
  } else {
    return List.generate(start - finish + 1, (i) => start - i);
  }
}

List<int> rangeWithStep(int start, int finish, int step) {
  var rangeList = range(start, finish);
  var stepList = <int>[];
  for (var i = 0; i < rangeList.length; i += step) {
    stepList.add(rangeList[i]);
  }
  return stepList;
}

void main() {
  print(rangeWithStep(1, 10, 2));
  print(rangeWithStep(10, 1, 2));
  print(rangeWithStep(1, 10, 3));
  print(rangeWithStep(10, 1, 3));
}
