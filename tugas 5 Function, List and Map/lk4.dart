import 'dart:io';

void main() {
  stdout.write("Masukkan sebuah angka: ");
  int angka = int.parse(stdin.readLineSync()!);
  int faktorial = hitungFaktorial(angka);
  print("$angka! = $faktorial");
}

int hitungFaktorial(int angka) {
  if (angka <= 0) {
    return 1;
  } else {
    int faktorial = 1;
    for (int i = angka; i >= 1; i--) {
      faktorial *= i;
    }
    return faktorial;
  }
}
