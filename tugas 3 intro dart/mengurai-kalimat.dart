void main() {
  var sentence = "I am Neskar Student";
  var exampleFirstWord = sentence[0];
  var exampleSecondWord = sentence.substring(2, 4);
  var thirdWord = sentence.substring(5, 11);
  var fourthWord = sentence.substring(12);

  print('First word: ' + exampleFirstWord);
  print('Second word: ' + exampleSecondWord);
  print('Third word: ' + thirdWord);
  print('Fourth word: ' + fourthWord);
}
