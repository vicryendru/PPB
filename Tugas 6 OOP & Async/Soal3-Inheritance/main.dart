import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';
import 'titan.dart';

void main() {
  var armorTitan = ArmorTitan();
  var attackTitan = AttackTitan();
  var beastTitan = BeastTitan();
  var human = Human();

  var armorTitanPowerPoint = Titan(3);
  var attackTitanPowerPoint = Titan(7);
  var beastTitanPowerPoint = Titan(4);
  var humanPowerPoint = Titan(10);

  armorTitanPowerPoint.powerPoint = 2;
  attackTitanPowerPoint.powerPoint = 8;
  beastTitanPowerPoint.powerPoint = 3;
  humanPowerPoint.powerPoint = 12;

  print("Armor Titan power point: ${armorTitanPowerPoint.powerPoint}");
  print("Attack Titan power point: ${attackTitanPowerPoint.powerPoint}");
  print("Beast Titan power point: ${beastTitanPowerPoint.powerPoint}");
  print("Human power point: ${humanPowerPoint.powerPoint}");

  print("Armor Titan terjang: ${armorTitan.terjang()}");
  print("Attack Titan punch: ${attackTitan.punch()}");
  print("Beast Titan lempar: ${beastTitan.lempar()}");
  print("Human kill all titans: ${human.killAllTitan()}");
}
