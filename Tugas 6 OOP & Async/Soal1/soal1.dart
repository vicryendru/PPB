class Segitiga {
  double alas;
  double tinggi;

  Segitiga(this.alas, this.tinggi);

  double hitungLuas() {
    return 0.5 * alas * tinggi;
  }
}

void main() {
  Segitiga segitiga = Segitiga(5, 10);

  double luasSegitiga = segitiga.hitungLuas();

  print("Luas segitiga: $luasSegitiga");
}
