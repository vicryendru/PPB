import 'dart:math';

class Lingkaran {
  double _jariJari = 0;

  Lingkaran(double jariJari) {
    this._jariJari = jariJari < 0 ? jariJari * -1 : jariJari;
  }

  double hitungLuas() {
    return pi * pow(_jariJari, 2);
  }

  double get jariJari => _jariJari;

  set jariJari(double jariJari) {
    this._jariJari = jariJari < 0 ? jariJari * -1 : jariJari;
  }
}

void main() {
  Lingkaran lingkaran = Lingkaran(5);

  print("Jari-jari: ${lingkaran.jariJari}");
  print("Luas lingkaran: ${lingkaran.hitungLuas()}");

  lingkaran.jariJari = -3;

  print("Jari-jari: ${lingkaran.jariJari}");
  print("Luas lingkaran: ${lingkaran.hitungLuas()}");
}
