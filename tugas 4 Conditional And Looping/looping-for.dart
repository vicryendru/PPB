import 'dart:io';

void main() {
  stdout.write('Masukkan batas perulangan: ');
  String input = stdin.readLineSync() ?? '';
  int batas = int.tryParse(input) ?? 0;

  for (int i = 1; i <= batas; i++) {
    if (i % 2 == 0) {
      print('I Love Coding');
    } else {
      print('I Love Neskar');
    }
    if (i % 5 == 0) {
      print('Neskar Always Number One !');
    }
  }
}
