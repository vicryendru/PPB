import 'dart:io';

void main() {
  stdout.write('Masukkan nilai: ');
  String input = stdin.readLineSync() ?? '';
  int nilai = int.tryParse(input) ?? 0;

  if (nilai >= 80) {
    print('A');
  } else if (nilai >= 70) {
    print('B');
  } else if (nilai >= 60) {
    print('C');
  } else {
    print('Tidak Lulus');
  }
}
