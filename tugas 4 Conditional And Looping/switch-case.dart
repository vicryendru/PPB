import 'dart:io';

void main() {
  stdout.write('Masukkan hari: ');
  String input = stdin.readLineSync()?.toLowerCase() ?? '';

  switch (input) {
    case 'senin':
      print('Jadwal hari ini: Matematika');
      break;
    case 'selasa':
      print('Jadwal hari ini: B. Indonesia');
      break;
    case 'rabu':
      print('Jadwal hari ini: B. Inggris');
      break;
    case 'kamis':
      print('Jadwal hari ini: PPKN');
      break;
    case 'jumat':
      print('Jadwal hari ini: Kejuruan');
      break;
    case 'sabtu':
    case 'minggu':
      print('Jadwal hari ini: Libur');
      break;
    default:
      print('Hari tidak valid');
  }
}
