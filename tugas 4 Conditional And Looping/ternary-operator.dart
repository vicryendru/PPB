import 'dart:io';

void main() {
  stdout.write('Apakah Anda ingin menginstall aplikasi? (Y/T): ');
  String input = stdin.readLineSync() ?? '';
  String output =
      (input == 'Y') ? 'Instalasi berjalan.' : 'Instalasi dibatalkan.';
  print(output);
}
